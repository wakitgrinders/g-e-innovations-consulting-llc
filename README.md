Wakit Grinders are the first ever blade-free herb grinder on the market. Wakits innovative technology allows you to easily grind your herb and separate thick stems without removing them first or grading them up.
Preserve the quality of your bud and medicinal properties without losing any herb or kief in the teeth. Wakits ball and chain prevent your expensive weed from getting stuck in the grinder. A huge advantage compared to other grinders on the market.
Curious about the quality of the grind? Wakit Grinder always delivers a consistent textured grind no matter how dry, moist, or sticky the weed. You have the control as to how coarse or fine you want your grind.
With a 5 second grind time, Wakit proves to be the fastest electric weed grinder on the market.

Website : https://wakitgrinders.com/